#!/bin/sh
# wait-for-service.sh

set -e

host="$1"
shift

echo "Health checking for a host $host is being started at the moment"

if curl --silent --connect-timeout 30 --retry 500 --retry-connrefused --retry-delay 5 "$host";
then
  echo "The host is up. Your command is being run ($@)"
  exec "$@"
else
  echo "Your service hasn't been prepared yet"
fi

