# wait-for-service

You can add this script to a docker compose file as a command to wait until service is up.

For more information: https://docs.docker.com/compose/startup-order/

